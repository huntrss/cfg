# Readme for my .config folder backup

Here are the basic steps to apply the backup on a machine:

1. Clone the repository bare:

    ```shell
    git clone --bare <git-repo-url> $HOME/.cfg
    ```

2. Define the alias in the current shell scope:

    ```shell
    alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
    ```

3. Checkout the content:

    ```shell
    config checkout
    ```

4. Resolve conflicts

5. Disable show untracked files:

    ```shell
    config config --local status.showUntrackedFiles no
    ```

6. Redo the tide configuration for fish, see [.config/fish/README.md]

## Start from scratch

Use the following steps to start from scratch:

```shell
git init --bare $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no
```

## Links

* [https://www.atlassian.com/git/tutorials/dotfiles](https://www.atlassian.com/git/tutorials/dotfiles)
* [git-format-patch](https://git-scm.com/docs/git-format-patch)
* [git-am](https://git-scm.com/docs/git-am)
* [How to Create and Apply a Patch in Git](https://www.git-tower.com/learn/git/faq/create-and-apply-patch/)
