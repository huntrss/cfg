-- required for alternative parser path
vim.opt.runtimepath:append("$HOME/.local/share/nvim/tree-sitter/parsers/")

-- Lazy package manager
-- Lazy installation
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- vim settings
require("vim_settings")

-- Lazy setup
require("lazy").setup("plugins")
