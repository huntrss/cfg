-- terminal
-- autocommand to always start in terminal mode
vim.api.nvim_create_autocmd({ "TermOpen" }, {
  -- command = "startinsert",
  group = vim.api.nvim_create_augroup("custom-term-open", { clear = true }),
  callback = function()
    vim.opt.number = false
    vim.opt.relativenumber = false
    vim.cmd.startinsert()
  end,
})
-- functions to toggle a horizontal or vertical terminal as split window
local state = {
  win = -1,
  buf = -1,
}
local toggle_window = function(create_window)
  if not vim.api.nvim_win_is_valid(state.win) then
    local win = create_window()
    if not vim.api.nvim_buf_is_valid(state.buf) then
      vim.cmd.terminal()
      local buf = vim.api.nvim_get_current_buf()
      vim.api.nvim_set_option_value("buflisted", false, { buf = buf })
      state.buf = buf
    end
    state.win = win
    vim.api.nvim_win_set_buf(win, state.buf)
  else
    vim.api.nvim_win_hide(state.win)
  end
end
local create_horizontal_window = function()
  vim.cmd.split()
  local height = math.floor(vim.o.lines * 0.25)
  local win = vim.api.nvim_get_current_win()
  vim.api.nvim_win_set_height(win, height)
  return win
end
local htoggle = function()
  return toggle_window(create_horizontal_window)
end
local create_vertical_window = function()
  vim.cmd.vsplit()
  local width = math.floor(vim.o.columns * 0.35)
  local win = vim.api.nvim_get_current_win()
  vim.api.nvim_win_set_width(win, width)
  return win
end
local vtoggle = function()
  return toggle_window(create_vertical_window)
end
-- keymap to open a terminal in a new window below
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>", { desc = "Leave terminal mode" })
vim.keymap.set("n", "<leader>th", htoggle, { desc = "open [t]erminal [h]orizontally" })
vim.keymap.set("n", "<leader>tv", vtoggle, { desc = "open [t]erminal [v]ertically" })
vim.keymap.set("n", "<leader>tt", function()
  vim.cmd.tabnew()
  vim.cmd.term()
end, { desc = "open [t]erminal in [t]ab" })
