# neovim configuration

This configuration was created based on this Youtube Video series:

[Neovim for newbs](https://www.youtube.com/watch?v=zHTeCSVAFNY&list=PLsz00TDipIffreIaUNk64KxTIkQaGguqn)

And extended based on this Youtube Video series:

[Advent of Neovim](https://www.youtube.com/watch?v=TQn2hJeHQbM&list=PLep05UYkc6wTyBe7kPjQFWVXTlhKeQejM)

## C# support links

* [csharp.nvim](https://github.com/iabdelkareem/csharp.nvim)
* [Making C# and OmniSharp play well with Neovim](https://nicolaiarocci.com/making-csharp-and-omnisharp-play-well-with-neovim/)
* [Coding C# in Neovim](https://web.archive.org/web/20230317233559/https://rudism.com/coding-csharp-in-neovim/)

## Link

Some interesting links:

* [Rust and Neovim are AMAZING together](https://www.youtube.com/watch?v=gihHLsClHF0)
* [rustacean.nvim](https://github.com/mrcjkb/rustaceanvim/)
* [Automagically formatting on save, with Neovim and Language Server Protocol (LSP)](https://www.jvt.me/posts/2022/03/01/neovim-format-on-save/)
* [Neovim and Git: SOLVED](https://www.youtube.com/watch?v=zOQMwWqdp9w)
* [Transform Your Neovim into a IDE: A Step-by-Step Guide](https://martinlwx.github.io/en/config-neovim-from-scratch/)
* [Everything you need to know to configure neovim using lua](https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/)
* [Combining LSPs and Telescope to find code references](https://neovim.substack.com/p/combining-lsps-and-telescope-to-find)
* [keys.nvim - A plugin that shows keystrokes](https://github.com/tamton-aquib/keys.nvim)
* [nvim-mdlink - provides additional functionality when working with markdown links](https://github.com/Nedra1998/nvim-mdlink)
* [The Only Video You Need to Get Started with Neovim](https://www.youtube.com/watch?v=m8C0Cq9Uv9o)
* [easytables.nvim](https://github.com/Myzel394/easytables.nvim)
* [nvim-bufferlist](https://github.com/kilavila/nvim-bufferlist)
* [syntweave - theme](https://github.com/samharju/synthweave.nvim)
