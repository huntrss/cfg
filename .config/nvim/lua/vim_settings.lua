-- basic configuration
-- tab configuration
vim.cmd("set autoindent expandtab tabstop=2 softtabstop=2 shiftwidth=2")
-- spelling
vim.cmd("set spell spelllang=en_us")

-- splitting windows
vim.opt.splitright = true
vim.opt.splitbelow = true

-- leader
vim.g.mapleader = " "

-- hybrid line number
vim.wo.number = true
vim.wo.relativenumber = true

-- keymaps for buffer handling
vim.keymap.set("n", "<leader>bd", "<CMD>bp<bar>sp<bar>bn<bar>bd<CR>", { desc = "[b]uffer: [d]elete" })
vim.keymap.set("n", "<leader>bn", vim.cmd.bnext, { desc = "[b]uffer: [n]ext" })
vim.keymap.set("n", "<leader>bp", vim.cmd.bprevious, { desc = "[b]uffer: [p]revious" })
vim.keymap.set("n", "<leader>bl", "<CMD>BufferListOpen<CR>", { desc = "[b]uffer: [l]ist" })
vim.keymap.set("n", "<leader>br", function()
  local buffer_name = vim.fn.input("Enter buffer name: ")
  vim.cmd("keepalt file " .. buffer_name)
end, { desc = "[b]uffer: [r]ename" })
vim.keymap.set("n", "<leader>bo", function()
  -- copied from https://stackoverflow.com/a/77799027
  -- License according to StackOverlow: CC BY-SA 4.0
  -- Code created by Branko: https://stackoverflow.com/users/8196612/branko
  local current_buf = vim.fn.bufnr()
  local current_win = vim.fn.win_getid()
  local bufs = vim.fn.getbufinfo({ buflisted = 1 })
  for _, buf in ipairs(bufs) do
    if buf.bufnr ~= current_buf then
      vim.cmd("silent! bdelete " .. buf.bufnr)
    end
  end
  vim.fn.win_gotoid(current_win)
end, { desc = "[b]uffer: delete [o]ther buffers" })

-- keymap for turning of search highlighting until the next search
vim.keymap.set("n", "<Esc>", vim.cmd.nohlsearch, { desc = "Turn off search highlighting until next search" })

-- unmap gcc keymap since it does the same as gc (toggle comment)
vim.api.nvim_del_keymap("n", "gcc")

-- keymap for open a definition in a new vertical splitted window
vim.keymap.set(
  "n",
  "gl",
  "<CMD>vsplit<CR><CMD>normal gd<CR>",
  { desc = "[g]o to definition in vertical sp[l]it window" }
)

-- disable arrow keys
vim.keymap.set("n", "<Up>", "<Nop>")
vim.keymap.set("n", "<Down>", "<Nop>")
vim.keymap.set("n", "<Right>", "<Nop>")
vim.keymap.set("n", "<Left>", "<Nop>")
vim.keymap.set("i", "<Up>", "<Nop>")
vim.keymap.set("i", "<Down>", "<Nop>")
vim.keymap.set("i", "<Right>", "<Nop>")
vim.keymap.set("i", "<Left>", "<Nop>")
vim.keymap.set("v", "<Up>", "<Nop>")
vim.keymap.set("v", "<Down>", "<Nop>")
vim.keymap.set("v", "<Right>", "<Nop>")
vim.keymap.set("v", "<Left>", "<Nop>")
vim.keymap.set("x", "<Up>", "<Nop>")
vim.keymap.set("x", "<Down>", "<Nop>")
vim.keymap.set("x", "<Right>", "<Nop>")
vim.keymap.set("x", "<Left>", "<Nop>")

-- quicklist keymaps
vim.keymap.set("n", "<M-j>", "<cmd>cnext<cr>", { desc = "Go the next quick list item" })
vim.keymap.set("n", "<M-k>", "<cmd>cprevious<cr>", { desc = "Go the previous quick list item" })
