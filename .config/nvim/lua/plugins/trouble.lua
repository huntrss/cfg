return {
  "folke/trouble.nvim",
  opts = {}, -- for default options, refer to the configuration section for custom setup.
  cmd = "Trouble",
  keys = {
    {
      "<leader>ctw",
      "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
      desc = "[c]ode: [t]rouble [w]indow",
    },
    {
      "<leader>ctn",
      [[<cmd>Trouble diagnostics next filter.buf=0<cr>
      <cmd>Trouble diagnostics jump filter.buf=0<cr>]],
      desc = "[c]ode: [t]rouble [n]ext",
    },
    {
      "<leader>ctp",
      [[<cmd>Trouble diagnostics prev filter.buf=0<cr>
      <cmd>Trouble diagnostics jump filter.buf=0<cr>]],
      desc = "[c]ode: [t]rouble [p]revious",
    },
    {
      "<leader>ctj",
      "<cmd>Trouble diagnostics jump filter.buf=0<cr>",
      desc = "[c]ode: [t]rouble [p]revious",
    },
  },
}
