return {
  {
    "nvim-telescope/telescope.nvim",
    tag = "0.1.5",
    dependencies = {
      "nvim-lua/plenary.nvim",
      { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    },
    config = function()
      -- require("telescope").setup({
      --   extensions = {
      --     fzf = {},
      --   },
      -- })
      require("telescope").load_extension("fzf")
      local builtin = require("telescope.builtin")
      -- keymaps
      vim.keymap.set("n", "<leader>ff", builtin.find_files, { desc = "[f]ind [f]iles" })
      vim.keymap.set("n", "<leader>fw", builtin.live_grep, { desc = "[f]ind [w]ords" })
      vim.keymap.set("n", "<leader>fb", builtin.buffers, { desc = "[f]ind open [b]uffers" })
      vim.keymap.set("n", "<leader>fh", builtin.help_tags, { desc = "[f]ind [h]elp tags" })
      vim.keymap.set("n", "<leader>fp", builtin.man_pages, { desc = "[f]ind man [p]ages" })
      vim.keymap.set("n", "<leader>fd", builtin.diagnostics, { desc = "[f]ind [d]iagnostics" })
      vim.keymap.set("n", "<leader>fr", builtin.lsp_references, { desc = "[f]ind [r]references" })
      vim.keymap.set("n", "<leader>fa", builtin.pickers, { desc = "[f]ind [a]gain" })
      vim.keymap.set("n", "<leader>fc", function()
        local opts = require("telescope.themes").get_ivy({
          cwd = vim.fn.stdpath("config"),
        })
        require("telescope.builtin").find_files(opts)
      end, { desc = "[f]ind neovim [c]onfig files" })
      -- go to definition in a new vertical splitted window
      -- copied from https://neovim.discourse.group/t/jump-to-definition-in-vertical-horizontal-split/2605/14
      -- This does not work for C# but is kept for reference
      -- vim.keymap.set(
      --   "n",
      --   "gl",
      --   '<CMD>lua require("telescope.builtin").lsp_definitions({jump_type="vsplit"})<CR>',
      --   { desc = "[g]o to definition in vertical sp[l]it window" }
      -- )
      require("config.telescope.multigrep").setup()
    end,
  },
  {
    "nvim-telescope/telescope-ui-select.nvim",
    config = function()
      require("telescope").setup({
        extensions = {
          ["ui-select"] = {
            require("telescope.themes").get_dropdown({}),
          },
        },
      })
      require("telescope").load_extension("ui-select")
    end,
  },
}
