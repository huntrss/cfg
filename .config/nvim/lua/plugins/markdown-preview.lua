return {
  "iamcco/markdown-preview.nvim",
  cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
  build = function()
    vim.fn["mkdp#util#install"]()
  end,
  keys = {
    {
      "<leader>cmp",
      ft = "markdown",
      "<cmd>MarkdownPreviewToggle<cr>",
      desc = "[c]ode: [m]arkdown [p]review",
    },
  },
  config = function()
    vim.cmd([[do FileType]])
  end,
}
