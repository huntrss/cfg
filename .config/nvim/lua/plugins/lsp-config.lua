return {
  {
    "iabdelkareem/csharp.nvim",
    dependencies = {
      "williamboman/mason.nvim", -- Required, automatically installs omnisharp
      "mfussenegger/nvim-dap",
      "Tastyep/structlog.nvim",  -- Optional, but highly recommended for debugging
      "saghen/blink.cmp",
    },
  },
  {
    "williamboman/mason.nvim",
    config = function()
      require("mason").setup()
      require("csharp").setup()
    end,
    opts = function(_, opts)
      opts.ensure_installed = opts.ensure_installed or {}
      vim.list_extend(opts.ensure_installed, { "markdownlint", "marksman" })
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    config = function()
      require("mason-lspconfig").setup({
        ensure_installed = { "lua_ls", "rust_analyzer", "zls" },
        automatic_installation = true,
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      {
        "folke/lazydev.nvim",
        ft = "lua", -- only load on lua files
        opts = {
          library = {
            -- See the configuration section for more details
            -- Load luvit types when the `vim.uv` word is found
            { path = "${3rd}/luv/library", words = { "vim%.uv" } },
          },
        },
      },
      "saghen/blink.cmp",
    },
    config = function()
      local capabilities = require("blink.cmp").get_lsp_capabilities()
      local lspconfig = require("lspconfig")
      lspconfig.lua_ls.setup({
        capabilities = capabilities,
      })
      lspconfig.zls.setup({
        capabilities = capabilities,
      })
      lspconfig.html.setup({
        capabilites = capabilities,
      })
      -- override the gD keymap
      vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { desc = "[g]o to [d]eclaration" })
      vim.keymap.set("n", "gd", vim.lsp.buf.definition, { desc = "[g]o to [d]efinition" })
      vim.keymap.set("n", "<leader>gi", vim.lsp.buf.implementation, { desc = "[g]o to [i]mplementation" })
      vim.keymap.set("n", "<leader>gt", vim.lsp.buf.type_definition, { desc = "[g]o to [t]ype definition" })
      vim.keymap.set("n", "<leader>gr", vim.lsp.buf.references, { desc = "[g]o to [r]eferences" })
      vim.keymap.set("n", "<leader>ch", vim.lsp.buf.hover, { desc = "[c]ode: [h]over" })
      vim.keymap.set("n", "<leader>cs", vim.lsp.buf.signature_help, { desc = "[c]ode: [s]ignature help" })
      vim.keymap.set("n", "<leader>cr", vim.lsp.buf.rename, { desc = "[c]ode: [r]ename" })
      vim.keymap.set("n", "<leader>cd", vim.diagnostic.open_float, { desc = "[c]ode: show line [d]iagnostic" })
      vim.keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, { desc = "[c]ode [a]ction" })
      vim.keymap.set("n", "<leader>cf", vim.lsp.buf.format, { desc = "[c]ode: [f]ormat" })
      -- format on save
      vim.api.nvim_create_autocmd("LspAttach", {
        callback = function(args)
          local client = vim.lsp.get_client_by_id(args.data.client_id)
          if not client then
            return
          end

          if client.supports_method("textDocument/formatting") then
            vim.api.nvim_create_autocmd("BufWritePre", {
              buffer = args.buffer,
              callback = function()
                vim.lsp.buf.format({ bufnr = args.buf, id = client.id })
              end,
            })
          end
        end,
      })
    end,
  },
}
