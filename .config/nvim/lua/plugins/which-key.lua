return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 300
  end,
  opts = {},
  config = function()
    local wk = require("which-key")
    wk.add(
      {
        { "<leader>b", group = "[b]uffer" },
        { "<leader>f", group = "[f]ind" },
        { "<leader>g", group = "[g]o to" },
        { "<leader>w", proxy = "<c-w>",                           group = "windows" },
        { "<leader>d", group = "[d]ebugging" },
        { "<leader>t", group = "[t]erminal" },
        { "<leader>c", group = "[c]ode (language server actions)" },
      }
    )
  end,
}
