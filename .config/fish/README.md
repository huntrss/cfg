# Fish config restore

Describes how to restore  the fish configuration.

1. Install [fisher](https://github.com/jorgebucaran/fisher)
2. Install [tide](https://github.com/IlanCosman/tide) with fisher but do not execute the setup
3. Use the below configure step to setup tide:

```shell
tide configure --auto --style=Rainbow --prompt_colors='True color' --show_time='24-hour format' --rainbow_prompt_separators=Angled --powerline_prompt_heads=Sharp --powerline_prompt_tails=Flat --powerline_prompt_style='One line' --prompt_spacing=Sparse --icons='Many icons' --transient=No
```
